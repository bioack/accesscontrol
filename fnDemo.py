from __future__ import division

import tensorflow as tf
import numpy as np
import argparse
import facenet
import os
import sys
import math
import pickle
import cv2
from sklearn.svm import SVC
from sklearn import metrics
from detect_face import *
from PIL import Image
from scipy import misc
from scipy import spatial


import datetime
import random
import hashlib
from cv2 import threshold

import dlib
import face_alignment
from skimage import io

#os.environ["CUDA_VISIBLE_DEVICES"] = ""

TEMPLATE = np.float32([
    (0.0792396913815, 0.339223741112), (0.0829219487236, 0.456955367943),
    (0.0967927109165, 0.575648016728), (0.122141515615, 0.691921601066),
    (0.168687863544, 0.800341263616), (0.239789390707, 0.895732504778),
    (0.325662452515, 0.977068762493), (0.422318282013, 1.04329000149),
    (0.531777802068, 1.06080371126), (0.641296298053, 1.03981924107),
    (0.738105872266, 0.972268833998), (0.824444363295, 0.889624082279),
    (0.894792677532, 0.792494155836), (0.939395486253, 0.681546643421),
    (0.96111933829, 0.562238253072), (0.970579841181, 0.441758925744),
    (0.971193274221, 0.322118743967), (0.163846223133, 0.249151738053),
    (0.21780354657, 0.204255863861), (0.291299351124, 0.192367318323),
    (0.367460241458, 0.203582210627), (0.4392945113, 0.233135599851),
    (0.586445962425, 0.228141644834), (0.660152671635, 0.195923841854),
    (0.737466449096, 0.182360984545), (0.813236546239, 0.192828009114),
    (0.8707571886, 0.235293377042), (0.51534533827, 0.31863546193),
    (0.516221448289, 0.396200446263), (0.517118861835, 0.473797687758),
    (0.51816430343, 0.553157797772), (0.433701156035, 0.604054457668),
    (0.475501237769, 0.62076344024), (0.520712933176, 0.634268222208),
    (0.565874114041, 0.618796581487), (0.607054002672, 0.60157671656),
    (0.252418718401, 0.331052263829), (0.298663015648, 0.302646354002),
    (0.355749724218, 0.303020650651), (0.403718978315, 0.33867711083),
    (0.352507175597, 0.349987615384), (0.296791759886, 0.350478978225),
    (0.631326076346, 0.334136672344), (0.679073381078, 0.29645404267),
    (0.73597236153, 0.294721285802), (0.782865376271, 0.321305281656),
    (0.740312274764, 0.341849376713), (0.68499850091, 0.343734332172),
    (0.353167761422, 0.746189164237), (0.414587777921, 0.719053835073),
    (0.477677654595, 0.706835892494), (0.522732900812, 0.717092275768),
    (0.569832064287, 0.705414478982), (0.635195811927, 0.71565572516),
    (0.69951672331, 0.739419187253), (0.639447159575, 0.805236879972),
    (0.576410514055, 0.835436670169), (0.525398405766, 0.841706377792),
    (0.47641545769, 0.837505914975), (0.41379548902, 0.810045601727),
    (0.380084785646, 0.749979603086), (0.477955996282, 0.74513234612),
    (0.523389793327, 0.748924302636), (0.571057789237, 0.74332894691),
    (0.672409137852, 0.744177032192), (0.572539621444, 0.776609286626),
    (0.5240106503, 0.783370783245), (0.477561227414, 0.778476346951)])

            
def main(args):
    # np.set_printoptions(threshold=np.inf)  
    image_size = 160
    with tf.Graph().as_default():
        # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.gpu_memory_fraction)
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.4)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        #sess = tf.Session(config=tf.ConfigProto(device_count = {'GPU': 0}, log_device_placement=False))
        facenet.load_model("20170512-110547.pb")
        # Get input and output tensors
        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        embedding_size = embeddings.get_shape()[1]
        
        
        #predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
        fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, enable_cuda=True,  enable_cudnn=True, flip_input=False)
        
        with sess.as_default():
            pnet, rnet, onet = create_mtcnn(sess, None)
    
        
        if args.mode == 'TRAIN':
            print('Start train dataset...')
            
            if not os.path.exists(args.data_dir):
                print("Wrong dir path.")
                exit()
            
            imgdata = []
            labelset = []
            labels = os.listdir(args.data_dir)
            total = 0
            class_names = []
            pretrained_labelset = []
            pretrained_emb_array = []
            filehash = []
            
            if os.path.exists("pretrained.pkl"):                
                with open('pretrained.pkl', 'rb') as infile:
                        pre_trained_emb = pickle.load(infile)
            else:
                pre_trained_emb = {}
                
            for i in range(len(labels)):
                label_path = os.path.join(args.data_dir, labels[i])
                class_names.append(labels[i])
                images = os.listdir(label_path)
                for j in range(len(images)):
                    image_path = os.path.join(label_path, images[j])
                    hs = calc_md5(image_path)
                    if hs in pre_trained_emb:
                        pretrained_labelset.append(i)
                        pretrained_emb_array.append(pre_trained_emb.get(hs))
                    else:
                        print("Calculate vectors of " + image_path)
                        alignedImage, _, _ = align_image(image_path, pnet, rnet, onet, image_size, fa)
                        # alignedImage = misc.imread(image_path)
                        if not alignedImage is None:
                            # tmppath = os.path.join(label_path, images[j] + "_160.jpg")
                            # misc.imsave(tmppath, alignedImage)
                            filehash.append(hs)
                            labelset.append(i)
                            alignedImage = facenet.prewhiten(alignedImage)
                            imgdata.append(alignedImage) 
                            total = total + 1
            imageset = np.zeros((total, image_size, image_size, 3))   
            for i in range(len(imgdata)):
                imageset[i, :, :, :] = imgdata[i]
                
                
            emb_array = np.zeros((total, embedding_size))           
            batch_size = 200
            nrof_batches = int(math.ceil(1.0 * total / batch_size))
            print('Number of embedding batches:' + str(nrof_batches))
            for i in range (nrof_batches):
                # print('Current batch number:' + str(i))
                start_index = i * batch_size
                end_index = min((i + 1) * batch_size, total)
                feed_dict = { images_placeholder:imageset[start_index:end_index], phase_train_placeholder:False }
                emb_array[start_index:end_index, :] = sess.run(embeddings, feed_dict=feed_dict)
            
            for i in range(total):
                pre_trained_emb[filehash[i]] = emb_array[i]
            
            if len(pretrained_emb_array) > 0:
                emb_array = np.vstack((emb_array, pretrained_emb_array))
            labelset = labelset + pretrained_labelset
            
            
            model = SVC(kernel='linear', probability=True)
            model.fit(emb_array, labelset)
            with open('training.pkl', 'wb') as outfile:
                    pickle.dump((model, class_names), outfile)
            with open('embs.dat', 'wb') as outfile:
                    pickle.dump((emb_array, labelset), outfile)
            with open('pretrained.pkl', 'wb') as outfile:
                    pickle.dump(pre_trained_emb, outfile)
            print('Done')
        elif args.mode == 'EVALUATE':
            if not os.path.exists(args.data_dir):
                print("Wrong dir path.")
                exit()
            print('Align images...')
            labels = os.listdir(args.data_dir)
            total_img = 0
            aligned_img = 0
            imgdata = []
            is_sideface = []
            file_idx_set = []
            img_path_set = []
            for i in range(len(labels)):
                label_path = os.path.join(args.data_dir, labels[i])
                photos = os.listdir(label_path)
                file_idx = []
                img_path = []
                for j in range(len(photos)):
                    total_img += 1
                    image_path = os.path.join(label_path, photos[j])
                    alignedImage, points, nf_faces = align_image(image_path, pnet, rnet, onet, image_size, fa)
                    if not alignedImage is None:
                        if nf_faces == 1:
                            alignedImage = facenet.prewhiten(alignedImage)
                            imgdata.append(alignedImage)
                            if is_side_face(points):
                                print('Side face image: ' + image_path)
                            is_sideface.append(is_side_face(points)) 
                            aligned_img += 1
                            img_path.append(image_path)
                            file_idx.append(aligned_img - 1)
                    else:
                        print("Picture " + image_path + " cannot be aligned")   
                    
                file_idx_set.append(file_idx)
                img_path_set.append(img_path)
            
            print('Total image: ' + str(total_img))
            print('Aligned image: ' + str(aligned_img))
            
            print('Calculate embeddings.')
            imageset = np.zeros((aligned_img, image_size, image_size, 3))   
            for i in range(len(imgdata)):
                imageset[i, :, :, :] = imgdata[i]
            # feed_dict = { images_placeholder:imageset, phase_train_placeholder:False }
            
            emb_array = np.zeros((aligned_img, embedding_size))           
            batch_size = 200
            nrof_batches = int(math.ceil(1.0 * aligned_img / batch_size))
            print('Number of embedding batches:' + str(nrof_batches))
            for i in range (nrof_batches):
                # print('Current batch number:' + str(i))
                start_index = i * batch_size
                end_index = min((i + 1) * batch_size, aligned_img)
                feed_dict = { images_placeholder:imageset[start_index:end_index], phase_train_placeholder:False }
                emb_array[start_index:end_index, :] = sess.run(embeddings, feed_dict=feed_dict)
            
            
            print('Start evaluate accuracy.')
            
            threshold = [0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
            #threshold = [0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6]
            #threshold = [0.75,0.8]
            TP = [0 for i in range(len(threshold))]
            FP = [0 for i in range(len(threshold))]
            TN = [0 for i in range(len(threshold))]
            FN = [0 for i in range(len(threshold))]
            TPR = [0 for i in range(len(threshold))]
            FPR = [0 for i in range(len(threshold))]
            Prec = [0 for i in range(len(threshold))]
            ACCU = [0 for i in range(len(threshold))]
            compared_pics = 0;
            
            for i in range(len(file_idx_set)):
                file_idx_one_label = file_idx_set[i]
                file_count = len(file_idx_one_label)
                if file_count < 2:
                    continue
                for k in range(file_count):
                    if is_sideface[file_idx_one_label[k]]:
                        #print("Side face: i:%d, k:%d"%(i,k))
                        continue
                    for m in range(k + 1, file_count):
                        if is_sideface[file_idx_one_label[m]]:
                            #print("Side face: i:%d, m:%d"%(i,m))
                            continue
                        compared_pics += 1 
                        emb_index_k = emb_array[file_idx_one_label[k]]
                        emb_index_m = emb_array[file_idx_one_label[m]]
                        posibility = 1 - spatial.distance.cosine(emb_index_k, emb_index_m)
                        for x in range(len(threshold)):                           
                            if posibility < threshold[x]:
                                #if x < 4:
                                print("threshold: %f, posibility: %f, src:%s, dst:%s"%(threshold[x], posibility, img_path_set[i][k],img_path_set[i][m]))
                                FN[x] += 1
                            else :
                                TP[x] += 1
            
            for i in range(len(file_idx_set)):
                file_idx_one_label = file_idx_set[i]
                file_count = len(file_idx_one_label)
                for j in range(file_count):
                    if is_sideface[file_idx_one_label[j]]:
                        # print("Side face: i:%d, j:%d"%(i,j))
                        continue
                    emb_index_j = emb_array[file_idx_one_label[j]]
                    for k in range(i + 1, len(file_idx_set)):
                        file_idx_snd_label = file_idx_set[k]
                        file_count_snd = len(file_idx_snd_label)
                        for m in range(len(file_idx_snd_label)):
                            if is_sideface[file_idx_snd_label[m]]:
                                # print("Side face: i:%d,, m:%d"%(i,m))
                                continue
                            emb_index_m = emb_array[file_idx_snd_label[m]]
                            compared_pics += 1 
                            posibility = 1 - spatial.distance.cosine(emb_index_j, emb_index_m)
                            for x in range(len(threshold)):
                                if posibility >= threshold[x]:
                                    print("threshold: %f, posibility: %f, src:%s, dst:%s"%(threshold[x], posibility, img_path_set[i][j],img_path_set[k][m]))
                                    FP[x] += 1
                                else:
                                    TN[x] += 1
            for x in range(len(threshold)):
                if not (TP[x] + FN[x]) == 0:
                    TPR[x] = TP[x] / (TP[x] + FN[x])
                else :
                    TPR[x] = -1
                if not FP[x] + TN[x] == 0:
                    FPR[x] = FP[x] / (FP[x] + TN[x])
                else:
                    FPR[x] = -1
                if not (TP[x] + FP[x]) == 0:
                    Prec[x] = TP[x] / (TP[x] + FP[x])
                else :
                    Prec[x] = -1
                    
                ACCU[x] = (TP[x] + TN[x]) / (TP[x] + TN[x] + FP[x] + FN[x])
            print('Finish evaluate accuracy.')
            print('Total compared pics: ' + str(compared_pics))
            print("Threshold: %r." % (threshold))
            print("True Positive: %r." % (TP))
            print("True Negative: %r." % (TN))
            print("False Positive: %r." % (FP))
            print("False Negative: %r." % (FN))
            
            print('Precision: %r.' % (Prec))
            print('Recall/TPR: %r.' % (TPR))   
            print('FPR: %r.' % (FPR))
            print('Accuracy: %r.' % (ACCU))  
            print('AUC: %r.' % (metrics.auc(FPR, TPR)))        
                            
        elif args.mode == 'RECOGNIZE':
            print('Start recognize face')
                
            with open('training.pkl', 'rb') as infile:
                    (model, class_names) = pickle.load(infile)        
            with open('embs.dat', 'rb') as infile:
                    (total_embs, labelset) = pickle.load(infile)
            
            url = args.stream
            # url=0
            cap = cv2.VideoCapture(url)
            
            if cap.isOpened() == False: 
                cap.open(url)
          
        
            skipFrameNum = 10
            currentFrameRound = skipFrameNum
            skipRecgNum = 3
            currentRound = skipRecgNum
            
            contTime = 100 
               
            while(True):                               
                ret, frame = cap.read()
                if frame is None:
                    contTime = contTime - 1
                    if contTime > 0:
                        continue
                    else:
                        print("Frame error!")
                        break
                    
                contTime = 100 
                
                # test code
                #frame = cv2.imread("D:\\Work\\Code\\eclipse\\python\\fndemo\\test.png")
                
                if(currentFrameRound == skipFrameNum):
                    '''
                    color_convFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV) 
                    color_convFrame[:,:,0] = cv2.equalizeHist(color_convFrame[:,:,0])
                    color_convFrame = cv2.cvtColor(color_convFrame,cv2.COLOR_YUV2RGB)
                      
                    '''
                    color_convFrame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                    alignedImage, det, points = align_frame(color_convFrame, pnet, rnet, onet, image_size, fa)
                    if not alignedImage is None:                    
                        alignedImage = facenet.prewhiten(alignedImage)
                        cv2.rectangle(frame, (det[:, 0], det[:, 1]), (det[:, 2], det[:, 3]), (0, 255, 0), 1)
                        for i in range(len(points)):
                            cv2.circle(frame, (points[i][0], points[i][1]), 1, (0, 0, 255), -1) 
                        #cv2.circle(frame, (points[0, :], points[5, :]), 2, (0, 0, 255), 2)
                        #cv2.circle(frame, (points[1, :], points[6, :]), 2, (0, 0, 255), 2)
                        #cv2.circle(frame, (points[2, :], points[7, :]), 2, (0, 0, 255), 2)
                        #cv2.circle(frame, (points[3, :], points[8, :]), 2, (0, 0, 255), 2)
                        #cv2.circle(frame, (points[4, :], points[9, :]), 2, (0, 0, 255), 2)
                        if currentRound == skipRecgNum:
                            imageset = np.zeros((1, image_size, image_size, 3))
                            imageset[0, :, :, :] = alignedImage
                            feed_dict = { images_placeholder:imageset, phase_train_placeholder:False }
                            emb_array = np.zeros((1, embedding_size))
                            start = datetime.datetime.now()
                            emb_array[0:1, :] = sess.run(embeddings, feed_dict=feed_dict)
                            end = datetime.datetime.now()  
                            #print("recognization duration:" + str(end-start))
                            # change svm to simple cosine distance
                            # predictions = model.predict_proba(emb_array)
                            # best_class_indices = np.argmax(predictions, axis=1)
                            # best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                            
                            best_probability = 0
                            best_class_index = -1
                            # for index in range(len(labelset)):
                            #    if labelset[index] == best_class_indices:
                            #        probability = 1-spatial.distance.cosine(total_embs[index], emb_array)
                            #        if probability > best_probability:
                            #            best_probability = probability
                            classCount = len(class_names)
                            class_sum_probability = [0] * classCount
                            class_sample_count = [0] * classCount
                            class_avg_probability = [0] * classCount
                            
                            for index in range(len(labelset)):
                                probability = 1 - spatial.distance.cosine(total_embs[index], emb_array)
                                # print (class_names[labelset[index]] + str(probability))
                                class_index = labelset[index]
                                class_sum_probability[class_index] = class_sum_probability[class_index] + probability
                                class_sample_count[class_index] = class_sample_count[class_index] + 1
                            
                            for index in range(classCount):
                                class_avg_probability[index] = class_sum_probability[index] / class_sample_count[index]
                                
                            for index in range(classCount):
                                if class_avg_probability[index] > best_probability:
                                    best_probability = class_avg_probability[index]
                                    best_class_index = index
                                
                            
                            class_name = class_names[best_class_index]
                            show_text = class_name + ' ' + str(best_probability)            
                            if best_probability >= 0.78 :
                                cv2.putText(frame, show_text, (det[:, 0], det[:, 1]), 0, 0.5, (0, 0, 255), 1)
                                capturedFolder = os.path.join('captured', class_name)
                                # os.system("door.exe")
                                # print("Open Sesame" + str(datetime.datetime.now()))
                                print(show_text + " " + str(datetime.datetime.now()))
                            else: 
                                cv2.putText(frame, 'unknown', (det[:, 0], det[:, 1]), 0, 0.5, (0, 0, 255), 1)
                                capturedFolder = os.path.join('captured', 'unknown')
                            
                            if not os.path.exists(capturedFolder):
                                os.makedirs(capturedFolder)
                            
                            rdm = str(random.randint(1, 9999))
                            capturedFileBg = "BG_" + show_text + "_" + rdm + '.png'
                            capturedFileBg = os.path.join(capturedFolder, capturedFileBg)
                            capturedFileMarkedBg = "MBG_" + show_text + "_" + rdm + '.png'
                            capturedFileMarkedBg = os.path.join(capturedFolder, capturedFileMarkedBg)
                            capturedFileHd = "HD_" + show_text + "_" + rdm + '.png'
                            capturedFileHd = os.path.join(capturedFolder, capturedFileHd)
                            marked_frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                            misc.imsave(capturedFileBg, color_convFrame)
                            misc.imsave(capturedFileMarkedBg, marked_frame)
                            misc.imsave(capturedFileHd, alignedImage)
                            
                            
                        currentRound = currentRound - 1
                        if currentRound == 0:
                            currentRound = skipRecgNum
                   
                currentFrameRound = currentFrameRound - 1
                if currentFrameRound == 0:
                    currentFrameRound = skipFrameNum
                            
                cv2.imshow('facenet', frame)
                if cv2.waitKey(10) & 0xFF == ord('q'):
                    break
            cap.release()
            cv2.destroyAllWindows()

            
def calc_md5(filepath):
    with open(filepath, 'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hs = md5obj.hexdigest()
        return hs

def simple_resize_frame(frame, target_width=320):
    src_width = frame.shape[1]
    src_height = frame.shape[0]
    ratio = src_width / src_height
    cropped = frame
    target_ratio = 1
    dltX = 0;
    dltY = 0;
    
    if src_width > target_width:
        if src_width >= target_width * 4:
            cropped_width = target_width * 4
            cropped_height = int(cropped_width / ratio)
            bb = np.zeros(4, dtype=np.int32)
            bb[0] = int((src_width - cropped_width) / 2)
            bb[2] = int((src_width + cropped_width) / 2) 
            bb[1] = int((src_height - cropped_height) / 2)
            bb[3] = int((src_height + cropped_height) / 2)
            dltX = bb[0]
            dltY = bb[1]
            cropped = frame[bb[1]:bb[3], bb[0]:bb[2], :]
            src_width = cropped_width
            src_height = cropped_height
        
        if(src_width > target_width):
            target_ratio = src_width / target_width
            target_height = int(src_height / target_ratio)
            cropped = cv2.resize(cropped, (target_width, target_height), interpolation=cv2.INTER_CUBIC)

    
    return dltX, dltY, target_ratio, cropped

def simple_resize_frame2(frame, target_width=320):
    src_width = frame.shape[1]
    src_height = frame.shape[0]
    ratio = src_width / src_height
    cropped = frame
    target_ratio = 1
    dltX = 0;
    dltY = 0;
    
    if src_width > target_width:
        target_ratio = src_width / target_width
        target_height = int(src_height / target_ratio)
        # cropped = cv2.resize(cropped, (target_width, target_height), interpolation=cv2.INTER_CUBIC)
        cropped = misc.imresize(cropped, (target_height, target_width), interp='bilinear')
    return dltX, dltY, target_ratio, cropped    
    
    
def is_side_face(points):
    left_eye_x = points[36][0]
    right_eye_x = points[45][0]
    nose_center_x = points[33][0]
    
    # left_eye_y = points[5][0]
    # right_eye_y = points[6][0]
    # nose_center_y = points[7][0]
    
    
    nose_left_x = nose_center_x - left_eye_x
    right_nose_x = right_eye_x - nose_center_x
    
    if nose_left_x <= 0:
        #print("nose_left_x < 0")
        return True
    if right_nose_x <= 0:
        #print("right_nose_x < 0")
        return True
    
    
    if nose_left_x / right_nose_x > 5:
        #print("nose_left_x / right_nose_x > 5")
        return True
    
    if right_nose_x / nose_left_x > 5:
        #print("right_nose_x / nose_left_x > 5")
        return True
    
    
    # print("left-right:" + str(left_eye_y - right_eye_y))
    # print("nose-left" + str(nose_center_y - right_eye_y))
                            
    return False
    
    
def simple_align_image(image, points):
    threshold = 0.5
    left_eye_x = points[0][0]
    right_eye_x = points[1][0]
    
    left_eye_y = points[5][0]
    right_eye_y = points[6][0]
    
    dltX = right_eye_x - left_eye_x
    dltY = right_eye_y - left_eye_y
    if dltY / dltX < threshold and dltY / dltX > -threshold:
        return image
    
    new_right_eye_x = left_eye_x + (dltY * dltY + dltX * dltX) ** 0.5
    new_right_eye_y = left_eye_y
    
    return image
                
def align_frame(frame, pnet, rnet, onet, image_size, fa): 
    margin = 16
    
    minsize = 20  # minimum size of face
    # threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
    threshold = [ 0.7, 0.8, 0.8 ] 
    factor = 0.709  # scale factor
    
    dltX, dltY, ratio, resized_frame = simple_resize_frame2(frame, target_width=480)
    #dltX = 0
    #dltY = 0
    #ratio = 1
    #resized_frame = frame
        
    if resized_frame.ndim < 2:
        return None, None
    if resized_frame.ndim == 2:
        resized_frame = facenet.to_rgb(resized_frame)
    resized_frame = resized_frame[:, :, 0:3]
    
    start2 = datetime.datetime.now()
    bounding_boxes, points = detect_face(resized_frame, minsize, pnet, rnet, onet, threshold, factor)
    end2 = datetime.datetime.now()
    #print("detect duration:" + str(end2-start2))
    
    nrof_faces = bounding_boxes.shape[0]
    if nrof_faces > 0:
        for i in range(0, nrof_faces):
            bounding_boxes[i][0] = bounding_boxes[i][0] * ratio + dltX
            bounding_boxes[i][1] = bounding_boxes[i][1] * ratio + dltX
            bounding_boxes[i][2] = bounding_boxes[i][2] * ratio + dltY
            bounding_boxes[i][3] = bounding_boxes[i][3] * ratio + dltY
        '''
        for i in range(0, nrof_faces):
            points[0][i] = points[0][i] * ratio + dltX
            points[1][i] = points[1][i] * ratio + dltX
            points[2][i] = points[2][i] * ratio + dltX
            points[3][i] = points[3][i] * ratio + dltX    
            points[4][i] = points[4][i] * ratio + dltX
            points[5][i] = points[5][i] * ratio + dltY
            points[6][i] = points[6][i] * ratio + dltY
            points[7][i] = points[7][i] * ratio + dltY
            points[8][i] = points[8][i] * ratio + dltY
            points[9][i] = points[9][i] * ratio + dltY
        '''
    if nrof_faces > 0:
        posibility = bounding_boxes[0][4]
        if posibility < 0.9:
            # print("posibility < 0.9")
            return None, None, None 
        
        #if is_side_face(points):
        #    return None, None, None 
        
        det = bounding_boxes[0:1, 0:4]
        img_size = np.asarray(frame.shape)[0:2]
        if nrof_faces > 1:
            bounding_box_size = (det[:, 2] - det[:, 0]) * (det[:, 3] - det[:, 1])
            img_center = img_size / 2
            offsets = np.vstack([ (det[:, 0] + det[:, 2]) / 2 - img_center[1], (det[:, 1] + det[:, 3]) / 2 - img_center[0] ])
            offset_dist_squared = np.sum(np.power(offsets, 2.0), 0)
            index = np.argmax(bounding_box_size - offset_dist_squared * 2.0)  # some extra weight on the centering
            det = det[index, :]
        det = np.squeeze(det)
        bb = np.zeros(4, dtype=np.int32)
        bb[0] = np.maximum(det[0] - margin / 2, 0)
        bb[1] = np.maximum(det[1] - margin / 2, 0)
        bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
        bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
        cropped = frame[bb[1]:bb[3], bb[0]:bb[2], :]
        
        dlib_recs = [dlib.rectangle(int(bb[0]), int(bb[1]), int(bb[2]), int(bb[3]))]
        start3 = datetime.datetime.now()
        shapes = fa.get_landmarks2(frame,dlib_recs)
        end3 = datetime.datetime.now()
        #print("alignment duration:" + str(end3-start3))
        if shapes is None or len(shapes) == 0:
            return None, None, 0
        
        shape = shapes[0]
        if is_side_face(shape):
            return None, None, None
        rows, cols, channel = frame.shape
        
        fst_idx = 36
        snd_idx = 45
        third_idx = 57
        
        outer_left_eye = [shape[fst_idx][0], shape[fst_idx][1]]
        outer_right_eye = [shape[snd_idx][0], shape[snd_idx][1]]
        bottom_lip = [shape[third_idx][0], shape[third_idx][1]]
        pts1 = np.float32([outer_left_eye, outer_right_eye, bottom_lip])
        pts2 = np.float32([[TEMPLATE[fst_idx][0] * cols, TEMPLATE[fst_idx][1] * rows], [TEMPLATE[snd_idx][0] * cols, TEMPLATE[snd_idx][1] * rows],[TEMPLATE[third_idx][0] * cols, TEMPLATE[third_idx][1] * rows]])
        
        
        '''
        left_eye = [(TEMPLATE[39][0] + TEMPLATE[36][0]) / 2 * cols, (TEMPLATE[39][1] + TEMPLATE[36][1]) / 2 * rows]
        right_eye = [(TEMPLATE[45][0] + TEMPLATE[42][0]) / 2 * cols, (TEMPLATE[45][1] + TEMPLATE[42][1]) / 2 * rows]
        
        lip_center = [(TEMPLATE[48][0] + TEMPLATE[64][0]) / 2 * cols, (TEMPLATE[48][1] + TEMPLATE[64][1]) / 2 * rows]
        pts1 = np.float32([[points[0][0], points[5][0]], [points[1][0], points[6][0]], [(points[3][0] + points[4][0]) / 2, (points[8][0] + points[9][0]) / 2]])
        pts2 = np.float32([left_eye, right_eye, lip_center])
        '''
        M = cv2.getAffineTransform(pts1, pts2)
        
        cropped = cv2.warpAffine(frame, M, (cols, rows))
        
        
        scaled = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
        return scaled, bounding_boxes[0:1, 0:4], shape
    return None, None, None 
    
           
def align_image(image_path, pnet, rnet, onet, image_size, fa):
    margin = 16
    
    minsize = 20  # minimum size of face
    # threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
    threshold = [ 0.7, 0.8, 0.8 ] 
    factor = 0.709  # scale factor
        
    img = misc.imread(image_path)
    if img.ndim < 2:
        return None
    if img.ndim == 2:
        img = facenet.to_rgb(img)
    img = img[:, :, 0:3]

    bounding_boxes, points = detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
    nrof_faces = bounding_boxes.shape[0]
    if nrof_faces > 0:
        det = bounding_boxes[0:1, 0:4]
        img_size = np.asarray(img.shape)[0:2]
        if nrof_faces > 1:
            bounding_box_size = (det[:, 2] - det[:, 0]) * (det[:, 3] - det[:, 1])
            img_center = img_size / 2
            offsets = np.vstack([ (det[:, 0] + det[:, 2]) / 2 - img_center[1], (det[:, 1] + det[:, 3]) / 2 - img_center[0] ])
            offset_dist_squared = np.sum(np.power(offsets, 2.0), 0)
            index = np.argmax(bounding_box_size - offset_dist_squared * 2.0)  # some extra weight on the centering
            det = det[index, :]
        det = np.squeeze(det)
        bb = np.zeros(4, dtype=np.int32)
        bb[0] = np.maximum(det[0] - margin / 2, 0)
        bb[1] = np.maximum(det[1] - margin / 2, 0)
        bb[2] = np.minimum(det[2] + margin / 2, img_size[1])
        bb[3] = np.minimum(det[3] + margin / 2, img_size[0])
        cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
        
        dlib_recs = [dlib.rectangle(int(bb[0]), int(bb[1]), int(bb[2]), int(bb[3]))]
        shapes = fa.get_landmarks2(img,dlib_recs)
        if shapes is None or len(shapes) == 0:
            return None, None, 0
        
        shape = shapes[0]
        rows, cols, channel = img.shape
        
        fst_idx = 36
        snd_idx = 45
        third_idx = 57
        
        outer_left_eye = [shape[fst_idx][0], shape[fst_idx][1]]
        outer_right_eye = [shape[snd_idx][0], shape[snd_idx][1]]
        bottom_lip = [shape[third_idx][0], shape[third_idx][1]]
        pts1 = np.float32([outer_left_eye, outer_right_eye, bottom_lip])
        pts2 = np.float32([[TEMPLATE[fst_idx][0] * cols, TEMPLATE[fst_idx][1] * rows], [TEMPLATE[snd_idx][0] * cols, TEMPLATE[snd_idx][1] * rows],[TEMPLATE[third_idx][0] * cols, TEMPLATE[third_idx][1] * rows]])
        
        '''
        
        left_eye = [(TEMPLATE[39][0] + TEMPLATE[36][0]) / 2 * cols, (TEMPLATE[39][1] + TEMPLATE[36][1]) / 2 * rows]
        right_eye = [(TEMPLATE[45][0] + TEMPLATE[42][0]) / 2 * cols, (TEMPLATE[45][1] + TEMPLATE[42][1]) / 2 * rows]
        
        lip_center = [(TEMPLATE[48][0] + TEMPLATE[64][0]) / 2 * cols, (TEMPLATE[48][1] + TEMPLATE[64][1]) / 2 * rows]
        pts1 = np.float32([[points[0][0], points[5][0]], [points[1][0], points[6][0]], [(points[3][0] + points[4][0]) / 2, (points[8][0] + points[9][0]) / 2]])
        pts2 = np.float32([left_eye, right_eye, lip_center])
        '''
       
        M = cv2.getAffineTransform(pts1, pts2)
        
        cropped = cv2.warpAffine(img, M, (cols, rows))
                
        scaled = misc.imresize(cropped, (image_size, image_size), interp='bilinear')
        return scaled, shape, nrof_faces
    return None, None, 0    
        
def gamma_correction(image, gamma):
    lut = [0 for i in range(256)]
    for i in range(0, 256):
        f = (i + 0.5) / 256
        fPrecompensation = 1 / gamma
        f = pow(f, fPrecompensation)
        lut[i] = int(f*256 - 0.5)
         
    rows, cols, channels = image.shape
    for i in range(0, rows):
        for j in range(0, cols):
            for k in range(0, channels):
                image[i,j,k] = lut[image[i,j,k]]
                
    return image
    
            
def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', type=str, choices=['TRAIN', 'RECOGNIZE', 'EVALUATE'], help='', default='RECOGNIZE')
    parser.add_argument('--data_dir', type=str, help='', default='')
    parser.add_argument('--stream', type=str, help='', default='rtsp://admin:admin123@10.168.1.37:554/Streaming/Channels/101')
    return parser.parse_args(argv)
        
if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
